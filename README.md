# App it Frontend Technical Test

# Architecture

- React-Native
- Realm (Local Database)

# Install

```bash
npm install
cd ios && pod install #Install ios dependance
```

# How to run

### IOS

```bash
npm run ios
```

### Android

```bash
npm run android
```
