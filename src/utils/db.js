import Realm from 'realm';

const RankSchema = {
  name: 'Rank',
  properties: {
    name: 'string',
    score: 'int',
  },
};

const realm = new Realm({ schema: [RankSchema] });

export const insertRank = async (name, score) => {
  realm.write(() => {
    realm.create('Rank', { name, score });
  });
};

export const getRank = async () => {
  return realm.objects('Rank');
};
