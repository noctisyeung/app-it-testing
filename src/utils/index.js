export const suffleArr = (x) => {
  if (Array.isArray(x)) {
    return x.sort(() => Math.random() - 0.5);
  }
  return [];
};

export const createArrList = (num) => {
  const arr = [];
  for (let i = 0; i < num; ) {
    arr.push(i);
    arr.push(i);
    i += 1;
  }
  return arr;
};

export const chunkArr = (arr, size) => {
  if (!arr) return [];
  const first = arr.slice(0, size);
  if (!first.length) {
    return arr;
  }
  return [first].concat(chunkArr(arr.slice(size, arr.length), size));
};
