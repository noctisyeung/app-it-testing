import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  gameAreaWrapper: {
    flexGrow: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  gameBoard: {
    aspectRatio: 1,
    flexShrink: 2,
    backgroundColor: '#e0e0e0',
    padding: 10,
    borderRadius: 10,
  },
  gameRow: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'nowrap',
  },
});

export default styles;
