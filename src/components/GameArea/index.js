import React from 'react';
import PropTypes from 'prop-types';
import { View, Dimensions } from 'react-native';
import * as Config from '../../config';
import GameCard from '../GameCard';
import InputModal from '../InputModal';
import { suffleArr, createArrList, chunkArr } from '../../utils';
import styles from './styles';

const TableRow = (props) => {
  const { children } = props;
  return <View style={styles.gameRow}>{children}</View>;
};

const GameArea = (props) => {
  const { handelScoreChange, score } = props;
  const [gameList, setGameList] = React.useState([]);
  const [pair, setPair] = React.useState([]);
  const [selected, setSelected] = React.useState([]);
  const [isPortrait, setPortrait] = React.useState(true);

  // This hook handle orientation change
  React.useEffect(() => {
    const getOrientation = () => {
      if (Dimensions.get('window').width < Dimensions.get('window').height) {
        setPortrait(true);
      } else {
        setPortrait(false);
      }
    };
    Dimensions.addEventListener('change', getOrientation);
    return () => Dimensions.removeEventListener('change', getOrientation);
  }, []);

  React.useEffect(() => {
    if (gameList.length === 0) {
      let newArr = createArrList(Config.COLORS.length);
      if (Array.isArray(newArr)) {
        newArr = suffleArr(newArr);
        if (newArr.length > 0) {
          setGameList(chunkArr(newArr, Config.ROW_NUM));
          setSelected([]);
        }
      }
    }
  }, [gameList]);

  React.useEffect(() => {
    if (pair.length === 2) {
      if (pair[0].k === pair[1].k) {
        setSelected((prevState) => [...prevState, ...pair]);
        handelScoreChange(5);
      } else {
        handelScoreChange(-1);
      }
      setTimeout(() => {
        setPair([]);
      }, 1000);
    }
  }, [pair, handelScoreChange]);

  const handleSelected = (k, id) => {
    const newPair = pair;
    if (newPair.length < 2) {
      newPair.push({ id, k });
      setPair([...newPair]);
    }
  };

  const handleCloseModal = () => {
    setSelected([]);
    setTimeout(() => {
      setGameList([]);
      setPair([]);
      handelScoreChange(-score);
    }, 300);
  };

  return (
    <>
      <View style={styles.gameAreaWrapper}>
        <View
          style={{
            ...styles.gameBoard,
            ...(isPortrait ? { width: '100%' } : { height: '85%' }),
          }}
        >
          {gameList.map((x, i) => (
            // eslint-disable-next-line react/no-array-index-key
            <TableRow key={i}>
              {x.map((y, k) => (
                <GameCard
                  // eslint-disable-next-line react/no-array-index-key
                  key={`${i}-${Config.COLORS[y].id}-${k}`}
                  id={`${i}-${Config.COLORS[y].id}-${k}`}
                  colorKey={Config.COLORS[y].id}
                  color={Config.COLORS[y].color}
                  handleSelected={handleSelected}
                  selected={selected}
                  pair={pair}
                  disabled={pair.length === 2}
                />
              ))}
            </TableRow>
          ))}
        </View>
      </View>
      <InputModal
        score={score}
        open={selected.length / 2 === Config.COLORS.length}
        onClose={handleCloseModal}
      />
    </>
  );
};

TableRow.propTypes = {
  children: PropTypes.node.isRequired,
};

GameArea.propTypes = {
  handelScoreChange: PropTypes.func.isRequired,
  score: PropTypes.number.isRequired,
};

export default GameArea;
