import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
  },
  rankModalWrapper: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  rankModal: {
    flex: 1,
    width: '90%',
    backgroundColor: '#FFF',
    borderRadius: 12,
    marginVertical: 30,
    padding: 10,
    alignItems: 'center',
  },
  rankTitleFont: {
    fontSize: 20,
  },
  tableHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#e0e0e0',
    paddingVertical: 10,
    marginTop: 15,
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    paddingVertical: 10,
    borderBottomColor: '#e0e0e0',
  },
  tableBody: {
    flexGrow: 1,
    width: '100%',
  },
  tableCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 5,
    flex: 1,
  },
  closeBtnWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#81c784',
    paddingVertical: 8,
    borderRadius: 5,
    width: 100,
  },
  btnFont: {
    color: '#FFF',
  },
  userModal: {
    width: 300,
    height: 200,
    padding: 10,
    backgroundColor: '#FFF',
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  scoreWrapper: {
    alignItems: 'center',
  },
  nameInputStyle: {
    height: 30,
    marginVertical: 5,
    width: 180,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 0,
    backgroundColor: '#eeeeee',
  },
  errorFont: {
    fontSize: 12,
    fontWeight: '500',
    color: '#f44336',
  },
  inputTitleFont: {
    fontSize: 23,
    fontWeight: '500',
  },
});

export default styles;
