import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { getRank } from '../../utils/db';
import styles from './styles';

const TABLE_HEADER = [
  { id: 'id', label: 'Rank' },
  { id: 'name', label: 'Name' },
  { id: 'score', label: 'Score' },
];

const createRank = (id, name, score) => {
  return { id, name, score };
};

const renderItem = ({ item }) => (
  <View key={item.id} style={styles.tableRow}>
    {TABLE_HEADER.map((y, i) => (
      // eslint-disable-next-line react/no-array-index-key
      <View key={`${item[y.id]}-${i}`} style={styles.tableCell}>
        <Text>{item[y.id]}</Text>
      </View>
    ))}
  </View>
);

const RankModal = (props) => {
  const { open, handleModal } = props;
  const [ranks, setRanks] = React.useState(null);

  React.useEffect(() => {
    if (open) {
      (async () => {
        try {
          const rankList = await getRank();
          if (rankList) {
            const r = rankList.sorted('score', true).map((x, i) => {
              return createRank(i + 1, x.name, x.score);
            });
            setRanks(r);
          }
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log(err);
        }
      })();
    }
  }, [open]);

  return (
    <Modal
      animationType="fade"
      transparent
      visible={open}
      supportedOrientations={['portrait', 'landscape']}
    >
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.rankModalWrapper}>
          <View style={styles.rankModal}>
            <Text style={styles.rankTitleFont}>Ranking</Text>
            <View style={styles.tableHeader}>
              {TABLE_HEADER.map((x) => (
                <View key={x.id} style={styles.tableCell}>
                  <Text>{x.label}</Text>
                </View>
              ))}
            </View>
            <View style={styles.tableBody}>
              {ranks && (
                <FlatList
                  style={{ flex: 1 }}
                  data={ranks || []}
                  // eslint-disable-next-line radix
                  keyExtractor={(item) => `${item.id}`}
                  renderItem={renderItem}
                />
              )}
            </View>
            <TouchableOpacity
              style={styles.closeBtnWrapper}
              onPress={() => handleModal(false)}
            >
              <Text style={styles.btnFont}>Close</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

RankModal.propTypes = {
  open: PropTypes.bool.isRequired,
  handleModal: PropTypes.func.isRequired,
};

export default RankModal;
