import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  imageWrapper: {
    width: 100,
    height: 50,
    resizeMode: 'contain',
  },
  scoreFont: {
    fontSize: 25,
    fontWeight: '700',
  },
  btnWrapper: {
    width: 100,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 8,
    borderRadius: 5,
    borderColor: '#81c784',
    borderWidth: 1,
  },
  btnFont: {
    color: '#81c784',
  },
});

export default styles;
