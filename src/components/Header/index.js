import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import styles from './styles';

const Header = (props) => {
  const { score, handleModal } = props;
  return (
    <View style={styles.headerWrapper}>
      <Image
        style={styles.imageWrapper}
        // eslint-disable-next-line global-require
        source={require('../../assets/images/logo.png')}
      />
      <Text style={styles.scoreFont}>{score}</Text>
      <TouchableOpacity
        style={styles.btnWrapper}
        onPress={() => handleModal(true)}
      >
        <Text style={styles.btnFont}>High Score</Text>
      </TouchableOpacity>
    </View>
  );
};

Header.propTypes = {
  score: PropTypes.number.isRequired,
  handleModal: PropTypes.func.isRequired,
};

export default React.memo(Header);
