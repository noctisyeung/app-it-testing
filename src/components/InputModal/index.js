import React from 'react';
import PropTypes from 'prop-types';
import {
  Modal,
  View,
  SafeAreaView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { insertRank } from '../../utils/db';
import styles from '../RankModal/styles';

const InputModal = (props) => {
  const { open, score, onClose } = props;
  const [name, setName] = React.useState('');
  const [err, setErr] = React.useState(null);

  const onPressDone = async () => {
    if (!name) {
      setErr('Please Enter Your Name');
    } else {
      try {
        await insertRank(name, score);
        setName('');
        setErr(null);
        if (onClose) {
          onClose();
        }
      } catch (error) {
        setErr('Cannot Insert To DB');
        // eslint-disable-next-line no-console
        console.error(error);
      }
    }
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={open}
      supportedOrientations={['portrait', 'landscape']}
    >
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.rankModalWrapper}>
          <View style={styles.userModal}>
            <Text style={styles.inputTitleFont}>WELL DONE!!!</Text>
            <View style={styles.scoreWrapper}>
              <Text>{`YOUR SCORE: ${score}`}</Text>
              <TextInput
                value={name}
                style={styles.nameInputStyle}
                onChangeText={(t) => setName(t)}
              />
              <Text style={styles.errorFont}>{err}</Text>
            </View>
            <TouchableOpacity
              style={styles.closeBtnWrapper}
              onPress={onPressDone}
            >
              <Text style={styles.btnFont}>Done</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </Modal>
  );
};

InputModal.propTypes = {
  open: PropTypes.bool.isRequired,
  score: PropTypes.number.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default InputModal;
