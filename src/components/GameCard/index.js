import React from 'react';
import PropTypes from 'prop-types';
import FlipCard from 'react-native-flip-card';
import { View, TouchableOpacity } from 'react-native';
import styles from './styles';

const GameCard = (props) => {
  const {
    colorKey,
    id,
    color,
    selected,
    pair,
    disabled,
    handleSelected,
  } = props;
  const _handleItemClick = () => {
    handleSelected(colorKey, id);
  };

  return (
    <TouchableOpacity
      style={styles.squareCard}
      disabled={
        disabled ||
        selected.findIndex((x) => x.id === id) > -1 ||
        pair.findIndex((x) => x.id === id) > -1
      }
      onPress={
        selected.findIndex((x) => x.id === id) > -1 ||
        pair.findIndex((x) => x.id === id) > -1 // eslint-disable-next-line operator-linebreak
          ? null
          : _handleItemClick
      }
    >
      <FlipCard
        style={{ margin: 0, padding: 0 }}
        flipHorizontal
        flipVertical={false}
        flip={
          selected.findIndex((x) => x.id === id) > -1 ||
          pair.findIndex((x) => x.id === id) > -1
        }
        clickable={false}
      >
        <View style={styles.squareCardFont} />
        <View style={{ flex: 1, backgroundColor: color }} />
      </FlipCard>
    </TouchableOpacity>
  );
};

GameCard.propTypes = {
  id: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  disabled: PropTypes.bool.isRequired,
  colorKey: PropTypes.string.isRequired,
  selected: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  pair: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  handleSelected: PropTypes.func.isRequired,
};

export default GameCard;
