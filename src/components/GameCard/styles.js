import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  squareCard: {
    flex: 1,
    aspectRatio: 1,
    margin: 5,
    borderRadius: 5,
    overflow: 'hidden',
  },
  squareCardFont: {
    flex: 1,
    backgroundColor: '#FFF',
  },
});

export default styles;
