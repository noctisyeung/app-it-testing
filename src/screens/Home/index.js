import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import { View, SafeAreaView } from 'react-native';
import Header from '../../components/Header';
import GameArea from '../../components/GameArea';
import RankModal from '../../components/RankModal';
import styles from './styles';

const Home = () => {
  const [score, setScore] = React.useState(0);
  const [openRank, setOpenRank] = React.useState(false);

  React.useEffect(() => {
    SplashScreen.hide();
  }, []);

  const handelScoreChange = React.useCallback((p) => {
    setScore((prevState) => prevState + p);
  }, []);

  const handleModal = React.useCallback((x) => {
    setOpenRank(x);
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.homeWrapper}>
        <Header score={score} handleModal={handleModal} />
        <GameArea handelScoreChange={handelScoreChange} score={score} />
        <RankModal open={openRank} handleModal={handleModal} />
      </View>
    </SafeAreaView>
  );
};

export default Home;
