import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  homeWrapper: {
    flex: 1,
    paddingHorizontal: 15,
  },
});

export default styles;
