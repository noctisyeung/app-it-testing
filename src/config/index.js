export const ROW_NUM = 4;
export const ITEM_IN_ROW = 4;
export const COLORS = [
  { id: 'red', color: '#ef9a9a' },
  { id: 'blue', color: '#90caf9' },
  { id: 'green', color: '#a5d6a7' },
  { id: 'yellow', color: '#fff59d' },
  { id: 'brown', color: '#8d6e63' },
  { id: 'orange', color: '#f57c00' },
  { id: 'pink', color: '#e91e63' },
  { id: 'teal', color: '#009688' },
];
